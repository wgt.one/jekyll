# Jekyll docker image

[Jekyll](https://jekyllrb.com) on alpine, with s3cmd and [minify](https://github.com/tdewolff/minify/tree/master/cmd/minify)

## Usage

`image: registry.gitlab.com/wgt.one/jekyll`
