FROM ruby:alpine

ADD https://svn.apache.org/viewvc/httpd/httpd/trunk/docs/conf/mime.types?revision=1810121&view=co /etc/mime.types
ADD https://github.com/tdewolff/minify/releases/download/v2.9.10/minify_linux_amd64.tar.gz /tmp/minify.tar.gz
RUN apk --no-cache add alpine-sdk && \
    apk --no-cache add s3cmd --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing && \
    cd /tmp && tar -xvzf minify.tar.gz && mv minify /bin/minify && chmod +x /bin/minify && \
    gem install jekyll && \
    apk del alpine-sdk
